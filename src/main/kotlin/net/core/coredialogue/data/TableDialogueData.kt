package net.core.coredialogue.data


data class TableDialogueData(
    val id: String, val characterId: String, val expression: String, val text: String,
    val options: String, val goTo: String, val values: String, val setCurrentDialogue: String,
    val signal: String, val function: String
)
