package net.core.coredialogue.helpers

import java.io.File
import java.nio.file.Paths


class FilesHelper {

    enum class Status {
        SUCCESS,
        ERROR,
        CANCEL
    }

    fun readFileAsText(filePath: String): String = File(filePath).readText(Charsets.UTF_8)

    fun writeToFile(filePath: String, text: String): Status {
        if(filePath.isBlank()) return Status.CANCEL

        try {
            File(filePath).printWriter().use { out ->
                out.print(text)
            }
        }catch(ex: Exception){
            println("Error while trying to write the file.")
            return Status.ERROR
        }

        return Status.SUCCESS
    }

    fun getFolderPath(fullPath: String?): String {

        if (fullPath.isNullOrBlank() || fullPath == "null") {
            return ""
        }

        return Paths.get(fullPath).parent.toString()
    }

    fun getFileFromFullPath(fullPath: String?): String {
        if (fullPath.isNullOrBlank() || fullPath == "null") {
            return ""
        }

        return Paths.get(fullPath).fileName.toString()
    }

    fun addExtension(fileName: String, extension: String): String {
        if(fileName.isBlank()) return fileName

        val ext = fileName.substringAfterLast('.', "")
        var completeFileName = fileName

        if(ext == ""){
            completeFileName += ".json"
        }

        return completeFileName
    }
}