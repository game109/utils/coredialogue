module net.core.coredialogue {
    requires javafx.controls;
    requires javafx.fxml;
    requires kotlin.stdlib;
    requires org.jfxtras.styles.jmetro;
    requires com.fasterxml.jackson.databind;
    requires com.fasterxml.jackson.kotlin;

    opens net.core.coredialogue to javafx.fxml;
    opens net.core.coredialogue.controllers to javafx.fxml;
    opens net.core.coredialogue.modelviews to javafx.base;
    opens net.core.coredialogue.data to com.fasterxml.jackson.databind;
    exports net.core.coredialogue.data to com.fasterxml.jackson.databind, kotlin.reflect;
    exports net.core.coredialogue;
    exports net.core.coredialogue.controllers to javafx.fxml;
}